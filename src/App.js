import Menu from './app/menu/Menu';
import './App.css';
import Header from './app/header/Header';
import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";

import Home from './app/home/Home';
import Profit from './app/profit/Profit';
import Planning from './app/planning/Planning';
import NoPage from './app/NoPage/NoPage';

function App() {
  return (
    <Router>
      <div className="container">
        <Header />
        <div className='menuHandling'>
          <Menu />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/profilt" element={<Profit />} />
            <Route path="/planning" element={<Planning />} />
            <Route path="*" element={<NoPage />} />
          </Routes>
        </div>
      </div>
    </Router>
  );
}

export default App;
