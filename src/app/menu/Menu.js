import { Outlet, Link } from "react-router-dom";

function Menu() {
    
    return (
        <>
            <nav className="menu">
                <ul className="menulist">
                    <li className="li">
                        <Link className="link" to="/">Home</Link>
                    </li>
                    <li className="li">
                        <Link className="link" to="/profilt">Sales & Profit</Link>
                    </li>
                    <li className="li">
                        <Link className="link" to="/planning">Sales Planning</Link>
                    </li>

                    <li className="li">
                        <Link className="link" to="/leakages">Opportunities & Leakages</Link>
                    </li>

                    <li className="li">
                        <Link className="link" to="/ticketing">Ticketing</Link>
                    </li>

                    <li className="li">
                        <Link className="link" to="/promos">Promos & Merchandising</Link>
                    </li>

                    <li className="li">
                        <Link className="link" to="/scorecard">Retailer Scorecard</Link>
                    </li>
                </ul>
            </nav>

            <Outlet />
        </>
    );

}

export default Menu;