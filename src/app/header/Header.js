import vector from '../../assests/images/vector.png'
import { Menu, MenuItem } from '@mui/material';
import ArrowDropDownSharpIcon from '@mui/icons-material/ArrowDropDownSharp';
import React, { useState } from 'react';

function Header() {
    const [anchorEl, setAnchorEl] = useState(false);
    const [mList,setMList] = useState(['Platform','Business','Category','Brand','ASIN','Tile','Time Frame']);
    const [pList,setPList] = useState(['Profile','My Account','LogOut']);
    const [bList,setBList] = useState(['Business1','Business2','Business3','Business4']);
    const [type,setType] = useState(0);

    const handleClick = (event) => {
        setType(mList.indexOf(event.currentTarget.__reactFiber$zr4sxl5buph.child.pendingProps.children));
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

const renderMenu = mList.map(item => 
    <div className='div' onClick={handleClick}>
    <text>{item}</text><ArrowDropDownSharpIcon />
    </div>
    );

 const renderList = type === 0 ? pList.map(item => <MenuItem onClick={handleClose}>{item}</MenuItem>) :
      bList.map(item => <MenuItem onClick={handleClose}>{item}</MenuItem>)
    
    return (
        <div>
            <img src={vector}></img>
            <ul className="header">
               {renderMenu}
            </ul>

            <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={anchorEl}
                onClose={handleClose}
                MenuListProps={{
                    'aria-labelledby': 'basic-button',
                }}>
                { anchorEl !== null&&
                   renderList
                }
            </Menu>
        </div>
    );
}
export default Header;